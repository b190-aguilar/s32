// importing the "http" module
const http = require("http");

// storing the 4000 in a variable port
const port = 4000;

// storing the create server method inside the server variable
const server = http.createServer((request, response) => {

	if (request.url === "/" && request.method === "GET"){
			response.writeHead(200, {"Content-Type":"text/plain"});
			response.end("Welcome to Booking System");	
		}

	else if (request.url === "/profile" && request.method === "GET"){
			response.writeHead(200, {"Content-Type":"text/plain"});
			response.end("Welcome to your profile!");	
		}	

	else if (request.url === "/profile" && request.method === "GET"){
			response.writeHead(200, {"Content-Type":"text/plain"});
			response.end("Welcome to your profile!");	
		}		

	else if (request.url === "/courses" && request.method === "GET"){
			response.writeHead(200, {"Content-Type":"text/plain"});
			response.end("Here’s our courses available");	
		}		

	else if (request.url === "/addCourse" && request.method === "POST"){
			response.writeHead(200, {"Content-Type":"text/plain"});
			response.end("Add a course to our resources.");	
		}	

	else if (request.url === "/updateCourse" && request.method === "PUT"){
			response.writeHead(200, {"Content-Type":"text/plain"});
			response.end("Update a course to our resources.");	
		}	

	else if (request.url === "/archiveCourse" && request.method === "DELETE"){
			response.writeHead(200, {"Content-Type":"text/plain"});
			response.end("Archive courses to our resources.");	
		}	

	else {
		response.writeHead(404, {"Content-Type":"text/plain"});
		response.end("Page does not exist.");	
	};

});
//using server and port variables
server.listen(port);

//confirmation that the server is ruuning
console.log(`Server now running at port ${port}`);

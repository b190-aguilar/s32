// importing the "http" module
const http = require("http");

// storing the 4000 in a variable port
const port = 4000;

// storing the create server method inside the server variable
const server = http.createServer((request, response) => {
/*
	create a route that will send the response "Data retrieved from the database" with 200 status code and text plain content type
*/

// HTTP method can be accessed via "method" property inside the "request" object; they are to be written in ALL CAPS
if (request.url === "/items" && request.method === "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Data retrieved from the database");	
	};

// POST REQUEST
// "POST" means that we will be adding/creating information
if (request.url === "/items" && request.method === "POST"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Data to be sent to the database");
	};


});
/*
	GET Method is one of the HTTPS methods that we will be using from this point
		GET Method means that we will be retrieving or reading information.
*/


/*
	POSTMAN
		since the Node.js application that we are building is a backend application, and there is no frontend application yet to process the requests, we will be using postman to process the request.

*/



//using server and port variables
server.listen(port);

//confirmation that the server is ruuning
console.log(`Server now running at port ${port}`);


// npx kill-port (portNumber)
	// terminates all currently running activities in a port.
	// useful for cases when the terminal is closed and the process is still running